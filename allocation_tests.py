
import unittest
from r_alloc import *

# Can be done with a few basic test cases
class InterferenceTest(unittest.TestCase):
    pass

# include tests that ensure that function returns a valid
# allocation scheme
# test that variables never overlap
class AllocationTest:
    pass

# Specific test cases
class SaturationTest(unittest.TestCase, AllocationTest):
    pass

if __name__ == '__main__':
    unittest.main()
