################################
# Author: Wesley Nuzzo

import unittest
from unittest import mock
from compiler import *

###########
# Original tests

# Add general resultUnchanged tests?
# Other general instance tests per function?

#from functools import reduce
#all_equal = lambda lst: reduce(lambda a, b: (a.val in (None, b) if a.out else False, b), lst, (True, None))[0]

#slightly more readable:
#from collections import namedtuple
#Helper = namedtuple('Helper', 'out val')
#def helper(a, b):
#    return Helper(out = a.val in (None, b) if a.out else False,
#                  val = b)
#def all_equal(lst):
#    return reduce(helper, lst, Helper(True, None))[0]

class TestUniquify(unittest.TestCase):
    ''' Test cases for the uniquify function'''

    def checkUniqueness(self, expr, used=set()):
        ''' Check that nested lets do not redefine variables.
        Also check that expr is in R1.'''
        ty = type(expr)
        if ty == R1.Var:
            self.assertIn(expr.name, used)
        elif ty in (R1.Program, R1.Negative, R1.Sum):
            for subexpr in expr:
                self.checkUniqueness(subexpr)
        elif ty == R1.Let:
            bindings = set(expr.bindings.keys())
            self.assertEqual(bindings & used, set())
            self.checkUniqueness(expr.body, used|bindings)
        else:
            self.assertIn(ty, (R1.Int, R1.Read))

    def checkProgram(self, program):
        ''' Check that the result of evaluating the program is the same,
        and that nested lets do not redefine the same variables.'''
        unique = uniquify(program)
        self.assertEqual(program.interpret(), unique.interpret())
        self.checkUniqueness(unique)
    
    def testIO(self):
        read = R1.Program(R1.Read())
        with mock.patch('_sys.readInt', return_value = 1):
            self.checkProgram(read)

    def testLiterals(self):
        for i in range(-8, 8):
            num = R1.Program(R1.Int(i))
            self.checkProgram(num)

    def testOperators(self):
        ''' Test uniquification of operators with and without contained lets.'''
        # unary operators
        for i in range(-8,8):
            neg = R1.Program(R1.Negative(R1.Int(i)))
            negLet = R1.Program(R1.Negative(R1.Let(R1.Var('x'),
                                                   x=R1.Int(i))))
            self.checkProgram(neg)
            self.checkProgram(negLet)

            # binary operators
            for j in range(-8,8):
                plus = R1.Program(R1.Sum(R1.Int(i),
                                         R1.Int(j)))
                plusLet = R1.Program(R1.Sum(R1.Let(R1.Var('x'),
                                                   x=R1.Int(i)),
                                            R1.Negative(R1.Let(R1.Var('x'),
                                                               x=R1.Int(j)))))
                self.checkProgram(plus)
                self.checkProgram(plusLet)

    def testFrames(self):
        ''' Variables defined in inner frames should not be defined in outer frames.

        Also check that the compiler fails when the input involves undefined
        variables.'''

        # Make sure programs with undefined variables are not compiled
        with self.assertRaises(R1.VarNotDefined):
            var = R1.Program(R1.Var('x'))
            uniquify(var)
        # May want to add a more elaborate generator for this
        for i in range(-8, 8):
            let = R1.Program(R1.Let(R1.Var('x'),
                                    x=R1.Int(i)))
            nest = R1.Program(R1.Let(R1.Let(R1.Var('x'),
                                            x=R1.Var('x')),
                                     x=R1.Int(i)))
            self.checkProgram(let)
            self.checkProgram(nest)

class TestFlatten(unittest.TestCase):
    ''' Check the operation of the flatten function, which converts from
    R1 to C0.'''

    def checkFlatness(self, program):
        ''' Check that the program is a series of flat assignment statements
        followed by a return statement.'''

        self.assertIsInstance(program, C0.Program)
        self.assertIsInstance(program.ret, C0.Var)

        # Check that each instruction is a flat assignment statement
        for instr in program.instrs:
            self.assertIsInstance(instr, C0.Assign)

            # Check the variable
            self.assertIn(instr.var, program.v)

            # Check the operator
            op = instr.expr
            self.assertIn(type(op), (C0.Var, C0.Int, C0.Read, C0.Negative, C0.Sum))
            if op == C0.Var:
                self.assertIn(subexpr.name, program.v)
            elif op in (C0.Negative, C0.Sum):
                for subexpr in op:
                    self.assertIn(type(subexpr), (C0.Var, C0.Int))
                    if type(subexpr) == C0.Var:
                        self.assertIn(subexpr.name, program.v)
        # Check that the return value is a variable
        self.assertIsInstance(program.ret, C0.Var)

    def checkProgram(self, program):
        ''' Check that the result of evaluating the program is the same,
        and that the flattened program is a flat C0 program.'''
        flat = flatten(program)
        self.assertEqual(program.interpret(), flat.interpret())
        self.checkFlatness(flat)

    def testIO(self):
        read = R1.Program(R1.Read())
        with mock.patch('_sys.readInt', return_value = 1):
            flatRead = flatten(read)
            self.checkProgram(read)

    def testLiterals(self):
        for i in range(-8, 8):
            num = R1.Program(R1.Int(i))
            flatNum = flatten(num)
            self.checkProgram(num)

    def testOperators(self):
        ''' Operators need to be flattened.
        May want to check that operators are always contained in an assignment statement'''
        for i in range(-8,8):
            # unary operators
            neg = R1.Program(R1.Negative(R1.Int(i)))
            negLet = R1.Program(R1.Negative(R1.Let(R1.Var('x'),
                                                   x=R1.Int(i))))
            self.checkProgram(neg)
            self.checkProgram(negLet)
            
            for j in range(-8,8):
                # binary operators
                plus = R1.Program(R1.Sum(R1.Int(i),
                                         R1.Int(j)))
                plusLet = R1.Program(R1.Sum(R1.Let(R1.Var('x'),
                                                   x=R1.Int(i)),
                                            R1.Negative(R1.Let(R1.Var('x'),
                                                               x=R1.Int(j)))))
                ##(program (+ (let ([x-0 -8]) x-0) (- (let ([x-0 -7]) x-0))))
                self.checkProgram(plus)
                self.checkProgram(plusLet)

    def testFrames(self):
        ''' Check that output uses assignment instead of nested lets.
        '''
        for i in range(-8, 8):
            let = R1.Program(R1.Let(R1.Var('x'),
                                    x=R1.Int(i)))
            nest = R1.Program(R1.Let(R1.Let(R1.Var('x'),
                                            x=R1.Var('y')),
                                y=R1.Int(i)))
            self.checkProgram(let)
            self.checkProgram(nest)

class TestSelectInstruction(unittest.TestCase):

    def checkForm(self, program):
        self.assertIsInstance(program, x86.Program)
        for instr in program.instrs:
            self.assertIn(type(instr),
                          (x86.Movq, x86.Negq, x86.Subq, x86.Addq,
                           x86.Pushq, x86.Popq))
            for arg in instr:
                self.assertIn(type(arg),
                              (x86.Int, x86.Reg, x86.Addr, x86.Var))
    def checkProgram(self, program):
        result = select_instr(program)
        self.assertEqual(program.interpret(), result.interpret())
        # Check that result is x86
        self.checkForm(result)

    def testLiterals(self):
        for i in range(-8, 8):
            num = C0.Program(('x'), C0.Var('x'),
                             C0.Assign('x', C0.Int(i)))
            self.checkProgram(num)

    def testOperators(self):
        for i in range(-8, 8):
            neg = C0.Program(('i', '-i'), C0.Var('-i'),
                             C0.Assign('i', C0.Int(i)),
                             C0.Assign('-i',
                                       C0.Negative(C0.Var('i'))))
            self.checkProgram(neg)
            for j in range(-8, 8):
                plus = C0.Program(('i', 'j', 'i+j'), C0.Var('i+j'),
                                  C0.Assign('i', C0.Int(i)),
                                  C0.Assign('j', C0.Int(j)),
                                  C0.Assign('i+j',
                                            C0.Sum(C0.Var('i'), C0.Var('j'))))
                self.checkProgram(plus)

    def testFrames(self):
        for i in range(-8,8):
            xy = C0.Program(('x', 'y'), C0.Var('y'),
                            C0.Assign('x', C0.Int(i)),
                            C0.Assign('y', C0.Var('x')))
            self.checkProgram(xy)
            for j in range(-8,8):
                xx = C0.Program(('x'), C0.Var('x'),
                                C0.Assign('x', C0.Int(i)),
                                C0.Assign('x', C0.Int(j)))
                self.checkProgram(xx)
                                
class TestAssignHomes(unittest.TestCase):
    def checkForm(self, program):
        self.assertIsInstance(program, x86.Program)
        for instr in program.instrs:
            self.assertIn(type(instr),
                          (x86.Movq, x86.Negq, x86.Subq, x86.Addq,
                           x86.Pushq, x86.Popq))
            for arg in instr:
                self.assertIn(type(arg), (x86.Reg, x86.Addr, x86.Int))

    def checkProgram(self, program, variables = ()):
        result = assign_homes(program, variables)
        self.assertEqual(program.interpret(), result.interpret())
        # Check that no variables are used
        self.checkForm(result)

    ########
    # Tests

    def testLiterals(self):
        for i in range(-8, 8):
            x = x86.Program(x86.Movq(x86.Int(i), x86.Reg('rax')))
            self.checkProgram(x, tuple('x'))

    def testOperators(self):
        for i in range(-8, 8):
            neg = x86.Program(x86.Movq(x86.Int(i), x86.Reg('rax')),
                              x86.Negq(x86.Reg('rax')))
            self.checkProgram(neg)
            for j in range(-8, 8):
                add = x86.Program(x86.Movq(x86.Int(i), x86.Reg('rax')),
                                  x86.Addq(x86.Int(j), x86.Reg('rax')))
                sub = x86.Program(x86.Movq(x86.Int(i), x86.Reg('rax')),
                                  x86.Subq(x86.Int(j), x86.Reg('rax')))
                self.checkProgram(add)
                self.checkProgram(sub)

    def testFrames(self):
        for i in range(8):
            for j in range(8):
                for k in range(8):
                    prog = x86.Program(x86.Movq(x86.Int(i), x86.Var('x')),
                                       x86.Movq(x86.Int(j), x86.Var('y')),
                                       x86.Movq(x86.Int(k), x86.Var('z')),
                                       x86.Addq(x86.Var('x'), x86.Var('y')),
                                       x86.Addq(x86.Var('y'), x86.Var('z')),
                                       x86.Addq(x86.Var('z'), x86.Var('x')),
                                       x86.Movq(x86.Var('x'), x86.Reg('rax')))
                    self.checkProgram(prog, tuple('xyz'))

class TestPatch(unittest.TestCase):
    def checkForm(self, program):
        self.assertIsInstance(program, x86.Program)
        for instr in program.instrs:
            self.assertIn(type(instr),
                          (x86.Movq, x86.Negq, x86.Subq, x86.Addq,
                           x86.Pushq, x86.Popq))
            if type(instr) in (x86.Movq, x86.Subq, x86.Addq):
                src, dest = instr
                self.assertFalse(isinstance(src, x86.Addr) and
                                 isinstance(dest, x86.Addr))
            for arg in instr:
                self.assertIn(type(arg), (x86.Reg, x86.Addr, x86.Int))

    def checkProgram(self, program):
        result = patch(program)
        self.assertEqual(program.interpret(), result.interpret())
        # Check that result has all other weirdnesses patched out
        self.checkForm(result)

    #######
    # Tests
    def testLiterals(self): 
        for i in range(-8, 8):
            x = x86.Program(x86.Movq(x86.Int(i), x86.RETURN))
            self.checkProgram(x)

    def testOperators(self):
        for i in range(-8, 8):
            neg = x86.Program(x86.Movq(x86.Int(i), x86.RETURN),
                              x86.Negq(x86.RETURN))
            self.checkProgram(neg)
            for j in range(-8, 8):
                add = x86.Program(x86.Movq(x86.Int(i), x86.RETURN),
                                  x86.Addq(x86.Int(j), x86.RETURN))
                sub = x86.Program(x86.Movq(x86.Int(i), x86.RETURN),
                                  x86.Subq(x86.Int(j), x86.RETURN))
                self.checkProgram(add)
                self.checkProgram(sub)

    def testFrames(self):
        for i in range(8):
            for j in range(8):
                prog = x86.Program(x86.Movq(x86.Addr(x86.Int(i)), x86.Addr(x86.Int(j))))
                self.checkProgram(prog)


class TestOutput(unittest.TestCase):
    ''' This part will require actually running the source code.'''
    pass

class TestPipeline(unittest.TestCase):

    def checkProgram(self, program):
        results = pipeline(program)
        outputs = [out for out in
                   map(lambda program: program.interpret(), results)]

#       print('########')
#       for result in results:
#           print (result)
#       print([out for out in outputs])
        self.assertEqual(len(set(outputs)), 1)
    
#   def testIO(self):
#       read = R1.Program(R1.Read())
#       with mock.patch('_sys.readInt', return_value = 1):
#           self.checkProgram(read)

    def testLiterals(self):
        for i in range(-8, 8):
            num = R1.Program(R1.Int(i))
            self.checkProgram(num)

    def testOperators(self):
        # unary operators
        for i in range(-8,8):
            neg = R1.Program(R1.Negative(R1.Int(i)))
            negLet = R1.Program(R1.Negative(R1.Let(R1.Var('x'),
                                                   x=R1.Int(i))))
            self.checkProgram(neg)
            self.checkProgram(negLet)

            # binary operators
            for j in range(-8,8):
                plus = R1.Program(R1.Sum(R1.Int(i),
                                         R1.Int(j)))
                plusLet = R1.Program(R1.Sum(R1.Let(R1.Var('x'),
                                                   x=R1.Int(i)),
                                            R1.Negative(R1.Let(R1.Var('x'),
                                                               x=R1.Int(j)))))
                self.checkProgram(plus)
                self.checkProgram(plusLet)

    def testFrames(self):
        # Make sure programs with undefined variables are not compiled
        with self.assertRaises(R1.VarNotDefined):
            var = R1.Program(R1.Var('x'))
            uniquify(var)
        # May want to add a more elaborate generator for this
        for i in range(-8, 8):
            let = R1.Program(R1.Let(R1.Var('x'),
                                    x=R1.Int(i)))
            nest = R1.Program(R1.Let(R1.Let(R1.Var('x'),
                                            x=R1.Var('x')),
                                     x=R1.Int(i)))
            self.checkProgram(let)
            self.checkProgram(nest)


if __name__ == '__main__':
    unittest.main()
