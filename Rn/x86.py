################################
# Author: Wesley Nuzzo
'''
Provides an interpreter for x86, along with some intermediate forms.
'''

from collections import namedtuple

REG_NUM = 16
MEMORY = 1024

####
# Functions and Labels
class Program:
    labels = {}
    def __init__(self, *instrs, **labels):
        self.reset()
        self.instrs = instrs
        for label in labels:
            self.labels[label] = labels[label]
    def __str__(self):
        out = '\n.globl main\nmain:\t'
        out += '\n\t'.join(str(instr) for instr in self.instrs)
        out += '\n\tretq\n'
        for label in self.labels:
            out += str(self.labels[label])
        return out
    def interpret(self):
        Reg.reset()
        Addr.reset()
        Var.reset()
        for instr in self.instrs:
            instr.interpret()
        return RETURN.getVal()
    @classmethod
    def reset(cls):
        cls.labels = {}

class Label:
    def __init__(self, name, *instrs):
        self.name = name
        self.instrs = instrs
    def __str__(self):
        out = '\n%s:\t' % self.name
        out += '\n\t'.join(str(instr) for instr in self.instrs)
        out += '\n\tretq\n'
        return out
    def interpret(self):
        for instr in self.instrs:
            instr.interpret()

class Callq(namedtuple('Callq', 'label')):
    def __str__(self):
        return 'callq %s:' % self.label
    def interpret(self):
        Program.labels[self.label].interpret()

####
# Types
class Int:
    def __init__(self, val):
        if not isinstance(val, int):
            raise TypeError
        self.val = val
    def __str__(self):
        return '$%d' % self.val
    def getVal(self):
        return self.val

####
# Operators

class Movq(namedtuple('Movq', 'src dest')):
    def __str__(self):
        return 'movq %s, %s' % self
    def interpret(self):
        self.dest.setVal(self.src.getVal())

class Negq(namedtuple('Movq', 'dest')):
    def __str__(self):
        return 'negq %s' % self
    def interpret(self):
        self.dest.setVal(-self.dest.getVal())

class Subq(namedtuple('Movq', 'src dest')):
    def __str__(self):
        return 'subq %s, %s' % self
    def interpret(self):
        self.dest.setVal(self.dest.getVal() - self.src.getVal())

class Addq(namedtuple('Movq', 'src dest')):
    def __str__(self):
        return 'addq %s, %s' % self
    def interpret(self):
        self.dest.setVal(self.dest.getVal() + self.src.getVal())

class Pushq(namedtuple('Pushq', 'src')):
    def __str__(self):
        return 'pushq %s' % self
    def interpret(self):
        base = Reg('rbp')
        base.setVal(base.getVal()-1)
        Addr(base, 0).setVal(self.src.getVal())

class Popq(namedtuple('Popq', 'dest')):
    def __str__(self):
        return 'popq %s' % self
    def interpret(self):
        base = Reg('rbp')
        self.dest.setVal(Addr(base, 0).getVal())
        base.setVal(base.getVal()+1)

####
# Memory

class Reg:
    reg = dict.fromkeys(('rsp', 'rbp', 'rax', 'rbx', 'rcx', 'rdx', 'rsi', 'rdi') +
                        tuple('r%d' % x for x in range(8,16)))
    def __init__(self, name):
        assert name in self.reg.keys()
        self.name = name
    def __str__(self):
        return '%%%s' % self.name
    @classmethod
    def reset(cls):
        for key in cls.reg:
            cls.reg[key] = None
        cls.reg['rbp'] = 3*MEMORY//4
        cls.reg['rsp'] = cls.reg['rbp']
    def setVal(self, val):
        self.reg[self.name] = val
    def getVal(self):
        return self.reg[self.name]

RETURN = Reg('rax')
STACK = Reg('rsp'), Reg('rbp')
SOURCE, DEST = Reg('rsi'), Reg('rdi')

class Addr:
    mem = [None]*MEMORY
    def __init__(self, index, offset = 0):
        assert isinstance(offset, int)
        self.index = index
        self.offset = offset
    def __str__(self):
        return '%d(%s)' % (self.offset*8, self.index)
    @classmethod
    def reset(cls):
        for i in range(len(cls.mem)):
            cls.mem[i] = None
    def setVal(self, val):
        self.mem[self.index.getVal()+self.offset] = val
    def getVal(self):
        return self.mem[self.index.getVal()+self.offset]

class Var:
    defined = {}
    def __init__(self, name):
        self.name = name
    def __str__(self):
        return self.name
    @classmethod
    def reset(cls):
        cls.defined = {}
    def setVal(self, val):
        self.defined[self.name] = val
    def getVal(self):
        return self.defined[self.name]

################################
# Read Function

# A truly effective implementation of this is impossible without use of the jump
# instruction.
# Not worth it.
read = Label('read', Movq(Int(255),
                          RETURN))
