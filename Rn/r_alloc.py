
from enum import Enum

import x86

'''
Register allocation:
Take as input a list of variables and some information about how they are used,
and then output a dictionary mapping each value to a register or a stack
location.
Goal is to minimize the number of variables assigned to stack locations, NOT
to minimize the actual number of stack locations used.
'''


Register = Enum('Register',
                 'rsp rbp rax rbx rcx rdx rsi rdi '+
                 ' '.join('r%d' % (x) for x in range(8,16)))

def gen_homes():
    ''' Generator for new variable homes. Starts with registers, then uses stack.'''
    for i in range(4, 17):
        yield x86.Reg(Register(i).name)
    i = 0
    while True:
        yield x86.Addr(x86.Reg('rbp'), offset = i)
        i -= 4

########
# Simplest approach
########

def always_spill(variables):
    ''' Put everything on the stack'''
    out = {}
    g = gen_homes()
    for i, v in enumerate(variables):
        out[v] = next(g)
    return out

########
### Saturation Algorithm
########

class Graph:
    def __init__(self, nodes=set()):
        self.data = dict.fromkeys(nodes, set())

    def check_nodes(self, *nodes):
        for node in nodes:
            if node not in self.data.keys():
                self.data[node] = set()

    def add_edge(self, start, end, directed = False):
        self.check_nodes(start, end)
        self.data[start] |= {end}
        if not directed:
            self.data[end] |= {start}

    def remove_edge(self, start, end, directed = False):
        self.data[start] -= {end}
        if not directed:
            self.data[end] -= {start}

    def get_adjacent(self, node):
        return self.data[node]

def annotate_liveness(instrs):
    ''' Return a list of tuples containing the instructions along with the set of variables
    live after the instruction.'''
    out = []
    live = set()
    for instr in reversed(instrs):
        # add instruction along with liveness annotation to output
        out.insert(0, (instr, live))

        # compute liveness for previous instruction
        ty = type(instr)
        
        # Dest
        if ty in (x86.Movq, x86.Popq):
            # These instructions overwrite dest, so dest cannot be live before them
            if type(instr.dest) == x86.Var:
                live -= {instr.dest}
        elif ty in (x86.Addq, x86.Subq, x86.Negq):
            # These instructions use dest as input, so dest must be live before them
            if type(instr.dest) == x86.Var:
                live |= {instr.dest}

        # Src
        if ty in (x86.Movq, x86.Addq, x86.Subq, x86.Pushq):
            # Src, if it's used, must be live
            if type(instr.src) == x86.Var:
                live |= {instr.src}
    return out

def calc_interference(instrs):
    ''' Given a list of instructions annotated with their liveness, compute the interference graph.'''

    out = Graph()
    for instr in instrs:
        instruction, live_after = instr
        ty = type(instr[0])
        if ty == x86.Movq:
            for var in live_after:
                if var not in instruction:
                    out.add_edge(instruction.dest, var)
        elif ty in (x86.Negq, x86.Addq, x86.Subq, x86.Popq):
            for var in live_after:
                if var != instruction.dest:
                    out.add_edge(instruction.dest, var)
    return out

def saturation_alloc(interference):
    ''' Allocate using graph coloring via the saturation method.

    interference: a dictionary representing the interference graph
    '''
    out = {}

    saturation = {}
    for var in interference.data.keys():
        saturation[var] = set()
    i=0
    for var in saturation:
        for i in range(4, 17):
            reg = x86.Reg(Register(i).name)
            if reg not in saturation[var]:
                out[var] = reg
                for var in interference.get_adjacent(var):
                    saturation[var] |= {reg}
                break
        if var not in out.keys():
            out[var] = x86.Addr(x86.Reg('rbp'), i)
            i -= 4
    return out

if __name__ == '__main__':
    graph = {'x': {'y'},
             'y': {'x','z'},
             'z': {'y'}}
    print(saturation_alloc(graph))
