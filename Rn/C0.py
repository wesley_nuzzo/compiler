################################
# Author: Wesley Nuzzo

from collections import namedtuple
import _sys

################################
# Exceptions

class NotDeclared(Exception):
    pass

class NotDefined(Exception):
    pass

################################
# Expressions

class Program:
    def __init__(self, v, ret, *instrs):
        self.v = v
        self.ret = ret
        self.instrs = list(instrs)
    def __str__(self):
        v = '(%s)' % ' '.join(var for var in self.v)
        instrs = ' '.join(str(instr) for instr in self.instrs)
        return '(program %s %s (ret %s))' % (v, instrs, self.ret)
    def interpret(self):
        env = dict.fromkeys(self.v)
        for instr in self.instrs:
            instr.interpret(env)
        return self.ret.interpret(env)

class Read(namedtuple('Read', '')):
    def __str__(self):
        return '(read)'
    def interpret(self, env):
        return _sys.readInt()

class Int:
    def __init__(self, val):
        if not isinstance(val, int):
            raise TypeError
        self.val = val
    def __str__(self):
        return str(self.val)
    def interpret(self, env):
        return self.val

class Negative(namedtuple('Negative', 'expr')):
    def __str__(self):
        return '(- %s)' % self
    def interpret(self, env):
        expr = self.expr.interpret(env)
        return -expr

class Sum(namedtuple('Sum', 'lhs rhs')):
    def __str__(self):
        return '(+ %s %s)' % self
    def interpret(self, env):
        lhs, rhs = (expr.interpret(env) for expr in self)
        return lhs + rhs

class Assign(namedtuple('Assign', 'var expr')):
    def __str__(self):
        return '(:= %s %s)' % self
    def interpret(self, env):
        if self.var not in env:
            raise NotDeclared
        env[self.var] = self.expr.interpret(env)

class Var:
    def __init__(self, name):
        self.name = name
    def __str__(self):
        return self.name
    def interpret(self, env):
        if self.name not in env:
            raise NotDeclared
        if env[self.name] == None:
            raise NotDefined
        return env[self.name]
