################################
# Author: Wesley Nuzzo
'''
This module provides an interpreter for the R1 language.

Each expression has a corresponding class.

<program> := (program <expr>)
<expr> := <read>|<int>|<neg>|<sum>|<let>|<var>
<read> := (read)
<int> := <digit>|<digit><int>
<digit> := 1|2|3|4|5|6|7|8|9|0
<neg> := (negative <expr>)
<sum> := (sum <expr> <expr>)
<let> := (let <binding> <expr>)
<binding> := ([<symbol> <expr>])
<var> := <symbol>
<symbol> := <char>|<char><symbol>
<char> := a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|x|y|z
'''

from collections import namedtuple
import _sys

################################
# Exceptions #

class VarNotDefined(Exception):
    pass

################################
# Expressions #

class Expression:
    pass

class Program(namedtuple('Program', 'body')):
    form = (Expression,)

    def __str__(self):
        return '(program %s)' % self

    def interpret(self):
        env = {}
        return self.body.interpret(env)

####
# I/O
class Read(namedtuple('Read', ''), Expression):
    form = ()

    def __str__(self):
        return '(read)'

    def interpret(self, env):
        return _sys.readInt()

####
# Types
class Type(Expression):
    pass

class Int(Type):
    t = int
    def __init__(self, val):
        if not isinstance(val, int):
            raise TypeError
        self.val = val
    def __str__(self):
        return str(self.val)
    def interpret(self, env):
        return self.val

####
# Operators
class Operator(Expression):
    pass

class Negative(namedtuple('Negative', 'expr'), Operator):
    form = Expression, Expression
    def __str__(self):
        return '(- %s)' % self
    def interpret(self, env):
        expr = self.expr.interpret(env)
        return -expr

class Sum(namedtuple('Sum', 'lhs rhs'), Operator):
    form = Expression, Expression
    def __str__(self):
        return '(+ %s %s)' % self
    def interpret(self, env):
        lhs, rhs = (expr.interpret(env) for expr in self)
        return lhs + rhs

####
# Variables and Frames

class Let(Expression):
    def __init__(self, body, **bindings):
        self.body = body
        self.bindings = bindings
    def __str__(self):
        bindings = ('[%s %s]' % (binding, self.bindings[binding])
                    for binding in self.bindings)
        return '(let (%s) %s)' % (''.join(bindings), self.body)
    def interpret(self, env):
        newEnv = env.copy()
        for binding in self.bindings:
            newEnv[binding] = self.bindings[binding].interpret(env)
        return self.body.interpret(newEnv)

class Var(Expression):
    form = (dict, Expression)
    def __init__(self, name):
        self.name = name
    def __str__(self):
        return self.name
    def interpret(self, env):
        try:
            return env[self.name]
        except KeyError as e:
            raise VarNotDefined(self.name) from e

####
# Symbols

class Symbol:
    def __init__(self, val):
        self.val = val
    def __str__(self):
        return self.val

class Gensym:
    count = 0
    def __init__(self):
        self.val = 'gensym-%s' % self.count
        self.count += 1
    def __str__(self):
        return self.val
