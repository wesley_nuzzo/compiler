################################
# Author: Wesley Nuzzo

import R1, C0, x86

import r_alloc

########
# Uniquify

def uniquify(expr, used = {}):
    if type(expr) in (R1.Read, R1.Int):
        return expr
    elif type(expr) in (R1.Program, R1.Negative, R1.Sum):
        return type(expr)(*(uniquify(subexpr)
                            for subexpr in expr))
    elif type(expr) == R1.Var:
        try:
            name = expr.name + '-' + str(used[expr.name])
            return R1.Var(name)
        except KeyError:
            raise R1.VarNotDefined
    elif type(expr) == R1.Let:
        new = used.copy()
        new_bindings = {}
        for binding in expr.bindings:
            if binding in used:
                new[binding] += 1
            else:
                new[binding] = 0
            new_binding = binding + '-' + str(new[binding])
            new_bindings[new_binding] = uniquify(expr.bindings[binding], used)
        return R1.Let(uniquify(expr.body, new),
                      **new_bindings)
    else:
        raise TypeError('uniquify: %s' % str(expr))

#######
# Flatten

# djennscymme
def djennscymme(count=[0]):
    out = count[0]
    count[0] += 1
    return str(out) +'-gsym'

def flatten(expr):
    ty = type(expr)
    ans = djennscymme()
    if ty == R1.Program:
        return flatten(expr.body)
    # Type
    elif ty == R1.Int:
        return C0.Program({ans}, C0.Var(ans), C0.Assign(ans, C0.Int(expr.val)))
    # Input
    elif ty == R1.Read:
        return C0.Program({ans}, C0.Var(ans), C0.Assign(ans, C0.Read()))
    # Operations
    elif ty == R1.Negative:
        subexpr = flatten(expr.expr)
        out= C0.Program(subexpr.v|{ans}, C0.Var(ans),
                          *(subexpr.instrs +
                            [C0.Assign(ans,
                                       C0.Negative(subexpr.ret))]))
        return out
    elif ty == R1.Sum:
        lhs, rhs = (flatten(x) for x in expr)
        instrs = lhs.instrs
        lval = lhs.ret
        if lhs.ret.name in rhs.v:
            lval = C0.Var(djennscymme())
            instrs.append(C0.Assign(lval.name, lhs.ret))
        instrs += rhs.instrs
        instrs.append(C0.Assign(ans, C0.Sum(lval,
                                            rhs.ret)))
        return C0.Program({ans, lval.name}|lhs.v|rhs.v, C0.Var(ans),
                          *instrs)
    # Variables
    elif ty == R1.Var:
        return C0.Program({expr.name}, C0.Var(expr.name))
    elif ty == R1.Let:
        _vars = set()
        instrs = []
        for binding in expr.bindings:
            _vars |= {binding}
            xe = flatten(expr.bindings[binding])
            # circle version may be preferrable for informative names
            # x-plus-lhs-minus-read
            _vars |= xe.v
            instrs +=  xe.instrs
            instrs.append(C0.Assign(binding, xe.ret))
        be = flatten(expr.body)
        return C0.Program(_vars|be.v, be.ret, *(instrs + be.instrs))
    else:
        raise Exception('flatten: unrecognized expression type %s' % str(type(expr)))

########
# Select Instruction

def select_instr(program):
    instrs = []
    for instr in program.instrs:
        assert type(instr) == C0.Assign
        dest = instr.var
        subinstr = instr.expr
        ty = type(subinstr)
        if ty == C0.Int:
            instrs.append(x86.Movq(x86.Int(subinstr.val),
                                   x86.Var(dest)))
        elif ty == C0.Negative:
            assert type(subinstr.expr) == C0.Var
            instrs.append(x86.Movq(x86.Var(subinstr.expr.name),
                                   x86.Var(dest)))
            instrs.append(x86.Negq(x86.Var(dest)))
        elif ty == C0.Sum:
            assert type(subinstr.lhs) == C0.Var
            assert type(subinstr.rhs) == C0.Var
            assert subinstr.lhs.name != dest
            instrs.append(x86.Movq(x86.Var(subinstr.rhs.name),
                                   x86.Var(dest)))
            instrs.append(x86.Addq(x86.Var(subinstr.lhs.name),
                                   x86.Var(dest)))
        elif ty == C0.Var:
            instrs.append(x86.Movq(x86.Var(subinstr.name),
                                   x86.Var(dest)))
    assert type(program.ret) == C0.Var
    instrs.append(x86.Movq(x86.Var(program.ret.name),
                           x86.RETURN))
    return x86.Program(*instrs)

########
# Assign Homes

def assign_homes(program, variables):
    k = len(variables)
    k = k+1 if k%2 else k

    annotated_instrs = r_alloc.annotate_liveness(program.instrs)
    interference = r_alloc.calc_interference(annotated_instrs)
    homes = r_alloc.saturation_alloc(interference)
#    homes = r_alloc.always_spill(variables)

    instrs = [x86.Pushq(x86.STACK[1]),
              x86.Movq(x86.STACK[0], x86.STACK[1]),
              x86.Subq(x86.Int(k), x86.STACK[0])]
    for line in program.instrs:
        args = [homes[arg.name] if type(arg) == x86.Var else arg
                for arg in line]
        instrs.append(type(line)(*args))
    instrs += [x86.Addq(x86.Int(k), x86.STACK[1]),
               x86.Popq(x86.STACK[0])]
    return x86.Program(*instrs)

########
# Patch

def patch(program):
    instrs = []
    for instr in program.instrs:
        if (len(instr) == 2):
            src, dest = instr
            if (isinstance(src, x86.Addr) and
                isinstance(dest, x86.Addr)):
                instrs += [x86.Movq(src, x86.RETURN),
                           type(instr)(x86.RETURN, dest)]
            else:
                instrs.append(instr)
        else:
            instrs.append(instr)
    return x86.Program(*instrs)

#######
# Output and Pipeline

def pipeline(program):
    steps = (uniquify, flatten, select_instr, assign_homes, patch)
    out = [program]
    for step in steps:
        if step == assign_homes:
            v = out[2].v
            out.append(step(out[-1], v))
        else:
            out.append(step(out[-1]))            
    return out
