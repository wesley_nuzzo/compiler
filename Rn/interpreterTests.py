################################
# Author: Wesley Nuzzo
'''
This file provides unit tests for the interpreters for
R1, C0, and x86.
'''

import unittest
from unittest import mock
import R1
import C0
import x86

class TestR1(unittest.TestCase):
    ''' Tests for the R1 interpreter'''

    def setUp(self):
        ''' Set up environments and mock objects for expressions.'''

        def increment():
            x=0
            while True:
                yield x
                x+=1
        
        incrementer = mock.Mock(side_effect=increment())
        alwaysOne = mock.Mock(return_value=1)

        self.incrementExpr = mock.NonCallableMock()
        self.incrementExpr.attach_mock(incrementer, 'interpret')

        self.emptyEnv = {}
        self.x1Env = {'x':1}

    def testProgram(self):
        ''' Programs are not passed an environment variable when they run.
        Check that interpreting is always equivalent to interpreting the subexpression
        with an empty env.'''
        prog = R1.Program(self.incrementExpr)
        for i in range(8):
            self.assertEqual(prog.interpret(), i)
            self.assertDictEqual(prog.body.interpret.call_args[0][0],
                                 self.emptyEnv)

    def testRead(self):
        ''' Read uses _sys.readInt() to read in an integer.
        Test that the interpreting read is always the same as the return
        value of this function.'''
        read = R1.Read()
        with mock.patch('_sys.readInt', side_effect=range(8)):
            for i in range(8):
                self.assertEqual(read.interpret(self.emptyEnv), i)

    def testInt(self):
        ''' Check that the result of interpreting an int is always the same as its value.
        Also check that it only accepts integers.'''
        for i in range(-8,8):
            num = R1.Int(i)
            self.assertEqual(num.interpret(self.emptyEnv), i)
        with self.assertRaises(TypeError):
            R1.Int(4.5)

    def testNegative(self):
        ''' Check that the result is always the negative of its expression.'''
        neg = R1.Negative(self.incrementExpr)
        for i in range(8):
            self.assertEqual(neg.interpret(self.emptyEnv), -i)

    def testSum(self):
        ''' Check that the result is always the sum of the expression results.'''
        s = R1.Sum(self.incrementExpr, self.incrementExpr)
        for i in range(4):
            self.assertEqual(s.interpret(self.emptyEnv), 4*i+1)

    def testLet(self):
        letX = R1.Let(self.incrementExpr, x=self.incrementExpr)
        letY = R1.Let(self.incrementExpr, y=self.incrementExpr)

        for i in range(8):
            n = 8*i
            self.assertEqual(letX.interpret(self.emptyEnv), n+1)
            self.assertDictEqual(letX.body.interpret.call_args[0][0], {'x':n})
            self.assertEqual(letY.interpret(self.emptyEnv), n+3)
            self.assertDictEqual(letY.body.interpret.call_args[0][0], {'y':n+2})
            self.assertEqual(letX.interpret(self.x1Env), n+5)
            self.assertDictEqual(letX.body.interpret.call_args[0][0], {'x':n+4})
            self.assertEqual(letY.interpret(self.x1Env), n+7)
            self.assertDictEqual(letY.body.interpret.call_args[0][0], {'x':1,
                                                                       'y':n+6})

    def testVar(self):
        var = R1.Var('x')
        for i in range(8):
            self.assertEqual(var.interpret({'x':i}), i)
        with self.assertRaises(R1.VarNotDefined):
            var.interpret(self.emptyEnv)

class TestC0(unittest.TestCase):

    def setUp(self):
        ''' Set up environments and mock objects for expressions.'''

        def increment():
            x=0
            while True:
                yield x
                x+=1
        
        incrementer = mock.Mock(side_effect=increment())
        alwaysOne = mock.Mock(return_value=1)

        self.incrementExpr = mock.NonCallableMock()
        self.incrementExpr.attach_mock(incrementer, 'interpret')

        self.emptyEnv = {}
        self.xEnv = {'x':None}
        self.x1Env = {'x':1}
   
    def testProgram(self):
        ''' The result of interpreting a C0 program is whatever its return
        statement evaluates to.'''
        prog = C0.Program(v=set(), ret=self.incrementExpr)
        for i in range(8):
            self.assertEqual(prog.interpret(), i)
            self.assertDictEqual(prog.ret.interpret.call_args[0][0],
                                 self.emptyEnv)
        prog = C0.Program(v={'x'}, ret=self.incrementExpr)
        for i in range(8,16):
            self.assertEqual(prog.interpret(), i)
            self.assertDictEqual(prog.ret.interpret.call_args[0][0],
                                 self.xEnv)

    def testRead(self):
        read = C0.Read()
        with mock.patch('_sys.readInt', side_effect=range(8)):
            for i in range(8):
                self.assertEqual(read.interpret(self.emptyEnv), i)

    def testInt(self):
        ''' Check that the result of interpreting an int is always the same as its value.
        Also check that it only accepts integers.'''
        for i in range(-8,8):
            num = C0.Int(i)
            self.assertEqual(num.interpret(self.emptyEnv), i)
        with self.assertRaises(TypeError):
            C0.Int(4.5)

    def testNegative(self):
        ''' Check that the result is always the negative of its expression.'''
        neg = C0.Negative(self.incrementExpr)
        for i in range(8):
            self.assertEqual(neg.interpret(self.emptyEnv), -i)

    def testSum(self):
        ''' Check that the result is always the sum of the expression results.'''
        s = C0.Sum(self.incrementExpr, self.incrementExpr)
        for i in range(4):
            self.assertEqual(s.interpret(self.emptyEnv), 4*i+1)

    def testAssign(self):
        asgn = C0.Assign('x', self.incrementExpr)
        with self.assertRaises(C0.NotDeclared):
            asgn.interpret(self.emptyEnv)
        with mock.patch('C0.Assign.interpret', side_effect=asgn.interpret):
            for i in range(8):
                self.assertIsNone(asgn.interpret(self.xEnv))
                self.assertDictEqual(asgn.interpret.call_args[0][0], {'x':i})
            for i in range(8,16):
                self.assertIsNone(asgn.interpret(self.x1Env))
                self.assertDictEqual(asgn.interpret.call_args[0][0], {'x':i})

    def testVar(self):
        var = C0.Var('x')
        with self.assertRaises(C0.NotDeclared):
            var.interpret(self.emptyEnv)
        with self.assertRaises(C0.NotDefined):
            var.interpret(self.xEnv)
        self.assertEqual(var.interpret(self.x1Env), 1)

class Testx86(unittest.TestCase):

    def setUp(self):
        x86.Reg.reset()
        x86.Addr.reset()
        x86.Var.reset()

        self.src = (x86.Addr(x86.Int(0), 0),
                    x86.Reg('rsi'),
                    x86.Var('source'),
                    x86.Int(8))
        for i in self.src[:3]:
            i.setVal(8)
        self.dest = (x86.Addr(x86.Int(0), 1),
                     x86.Reg('rdi'),
                     x86.Var('destination'))
        for i in self.dest:
            i.setVal(15)

        stack = x86.MEMORY // 2
        x86.Reg('rsp').setVal(stack)
        x86.Reg('rbp').setVal(stack)

    ## Code
    def testProgram(self):
        '''Running a program results in executing its instructions
        and then returning whatever is in %rax.'''
        for i in range(8):
            prog = x86.Program(x86.Movq(x86.Int(i),
                                        x86.Reg('rax')))
            self.assertEqual(prog.interpret(), i)
 
    def testLabel(self):
        ''' After calling a label, its result is always returned in rax.'''
        for i in range(8):
            label = x86.Label('label', x86.Movq(x86.Int(i),
                                                x86.Reg('rax')))
            prog = x86.Program(x86.Callq('label'),
                               label = label)
            self.assertEqual(prog.interpret(), i)

    ## Literals
    def testInt(self):
        for i in range(-8,8):
            num = x86.Int(i)
            self.assertEqual(num.getVal(), i)
        with self.assertRaises(TypeError):
            x86.Int(4.5)

    ## Operators
    # dest: Address, Variable, or Register
    # src: Address, Variable, Register, or Int
    def testMovq(self):
        for src in self.src:
            for dest in self.dest:
                mov = x86.Movq(src, dest)
                mov.interpret()
                self.assertEqual(dest.getVal(), src.getVal())

    def testNegq(self):
        for dest in self.dest:
            initial = dest.getVal()
            neg = x86.Negq(dest)
            neg.interpret()
            self.assertEqual(initial, -dest.getVal())

    def testSubq(self):
        for src in self.src:
            for dest in self.dest:
                diff = dest.getVal() - src.getVal()
                sub = x86.Subq(src, dest)
                sub.interpret()
                self.assertEqual(dest.getVal(), diff)

    def testAddq(self):
        for src in self.src:
            for dest in self.dest:
                _sum = src.getVal() + dest.getVal()
                add = x86.Addq(src, dest)
                add.interpret()
                self.assertEqual(dest.getVal(), _sum)

    def testStack(self):
        push = (x86.Pushq(x86.Int(i)) for i in range(8))
        pop = (x86.Popq(x86.Reg('rax')) for i in range(8))
        for p in push:
            p.interpret()
        stackLen = x86.Reg.reg['rsp']-x86.Reg.reg['rbp']
        self.assertEqual(stackLen, 8)
        for i, p in enumerate(pop):
            p.interpret()
            self.assertEqual(x86.Reg('rax').getVal(), 7-i)
        stackLen = x86.Reg.reg['rsp']-x86.Reg.reg['rbp']
        self.assertEqual(stackLen, 0)

    ## Memory
    def testReg(self):
        self.assertEqual(len(x86.Reg.reg.keys()), x86.REG_NUM)
        for i in range(8):
            for key in x86.Reg.reg.keys():
                x86.Reg(key).setVal(i)
                self.assertEqual(x86.Reg(key).getVal(), i)

    def testAddr(self):
        self.assertEqual(len(x86.Addr.mem), x86.MEMORY)
        for i in range(8):
            for j in range(8):
                front, back = x86.Int(i), x86.Int(x86.MEMORY-i)
                x86.Addr(front, 0).setVal(j)
                x86.Addr(back, -1).setVal(j)
                self.assertEqual(x86.Addr(front, 0).getVal(), j)
                self.assertEqual(x86.Addr(back, -1).getVal(), j)

    def testVar(self):
        for name in 'xyz':
            var = x86.Var(name)
            for i in range(-8, 8):
                var.setVal(i)
                self.assertEqual(var.getVal(), i)

if __name__ == '__main__':
    unittest.main()
