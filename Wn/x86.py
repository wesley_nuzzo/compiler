
from collections import namedtuple
import Wsys

class Program:
    def __init__(self, *body, **labels):
        self.body=body
        self.labels = labels
    def __str__(self):
        return '.globl main\nmain:\t%s\n\tretq' % '\n\t'.join(str(line)
                                                            for line in self.body)
    def interpret(self):
        Register.registers['rsi'] = len(Address.memory) // 2
        #print(self)
        for line in self.body:
            line.interpret()
        return True if Register.registers['rax'] else False

class Label:
    def __init__(self, name, *body):
        self.name = name
        self.body = body
    def __str__(self):
        return '\n%s:%s\n\tretq' % (name, self.body)
    def interpret(self):
        for line in self.body:
            line.interpret()
        Register.registers['rax'] = -1 if Wsys.readTF() else 0

class Callq(namedtuple('Callq', 'label')):
    def __str__(self):
        return '\tcallq %s\n' % self.label.name
    def interpret(self):
        self.label.interpret()
    
# Types
class Int(namedtuple('Int', 'val')):
    def __str__(self):
        return '$%s' % self
    def get(self):
        return self.val

class Bool(namedtuple('Bool', 'val')):
    def __str__(self):
        return '$%d' % (-1 if self.val else 0)
    def get(self):
        return self.val

# Variables
class Register(namedtuple('Register', 'name')):
    names = ('rsp','rbp','rax','rbx','rcx','rdx','rsi','rdi',
             'r8','r9','r10','r11','r12','r13','r14','r15')
    # add check that name is in names?
    registers = dict.fromkeys(names) # can set to 0 if necessary
    # advantage of using None is that it prevents assumptions
    def __str__(self):
        return '%%%s' % self.name
    def set(self, val):
        self.registers[self.name] = val
    def get(self):
        return self.registers[self.name]

class Address(namedtuple('Address', 'offset register')):
    memory=[0]*65536
    def __str__(self):
        return '%s(%s)' % self
    def set(self, val):
        self.memory[self.offset+self.register.get()]=val
    def get(self):
        return self.memory[self.offset+self.register.get()]

class Var(namedtuple('Var', 'name')):
    env = {}
    def __str__(self):
        return self.name
    def set(self, val):
        self.env[self.name]=val
    def get(self):
        return self.env[self.name]

## Operators

class Movq(namedtuple('Movq', 'lhs rhs')):
    def __str__(self):
        return 'movq %s, %s' % self
    def interpret(self):
        self.rhs.set(self.lhs.get())

# boolean
class Andq(namedtuple('Andq', 'lhs rhs')):
    def __str__(self):
        return 'andq %s, %s' % self
    def interpret(self):
        self.rhs.set(self.lhs.get() & self.rhs.get())

class Orq(namedtuple('Orq', 'lhs rhs')):
    def __str__(self):
        return 'orq %s, %s' % self
    def interpret(self):
        self.rhs.set(self.lhs.get() | self.rhs.get())

class Notq(namedtuple('Notq', 'dest')):
    def __str__(self):
        return 'notq %s' % self
    def interpret(self):
        self.dest.set(~self.dest.get())

# integer
class Addq(namedtuple('Addq', 'lhs rhs')):
    def __str__(self):
        return 'addq %s, %s' % self
    def interpret(self):
        self.rhs.set(self.lhs.get() + self.rhs.get())

class Subq(namedtuple('Subq', 'lhs rhs')):
    def __str__(self):
        return 'subq %s, %s' % self
    def interpret(self):
        self.rhs.set(self.rhs.get() - self.lhs.get())

class Negq(namedtuple('Negq', 'dest')):
    def __str__(self):
        return 'negq %s' % self
    def interpret(self):
        self.dest.set(-self.dest.get())

# other
class Pushq(namedtuple('Pushq', 'reg')):
    def __str__(self):
        return 'pushq %s' % self
    def interpret(self):
        pass

class Popq(namedtuple('Popq', 'reg')):
    def __str__(self):
        return 'popq %s' % self
    def interpret(self):
        pass
