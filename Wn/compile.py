
import W1
import C0
import x86

def uniquify(expr, frame = {}, depth = 0):
    # base cases
    if isinstance(expr, W1.Bool) or isinstance(expr, W1.Read):
        return expr
    elif isinstance(expr, W1.Var):
        try:
            return W1.Var(expr.name+'-'+str(frame[expr.name]))
        except KeyError:
            raise KeyError('Variable %s is not defined' % expr.name)
    # recursive cases
    elif isinstance(expr, W1.Let):
        newFrame = frame.copy()
        newBindings = {}
        # Note edge case (let ([x x]) ...)
        for binding in expr.bindings:
            newFrame[binding] = depth
            newBindings[binding+'-'+str(depth)] = uniquify(expr.bindings[binding],
                                                           frame, depth+1)
        return W1.Let(newBindings, uniquify(expr.body, newFrame, depth+1))
    else:
        return type(expr)(*[uniquify(subexpr, frame, depth+1) for subexpr in expr])

# djennscymme
def gensym(last=[0]):
    last[0]=last[0]+1
    return str(last[0])+'-gsym'

def flatten(expr):
    ans = gensym()
    if isinstance(expr, W1.Program):
        return flatten(expr.body)
    # Type
    elif isinstance(expr, W1.Bool):
        return C0.Program({ans}, ans, C0.Assign(ans, C0.Bool(expr.val)))
    # Input
    elif isinstance(expr, W1.Read):
        return C0.Program({ans}, ans, C0.Assign(ans, C0.Read()))
    # Operations
    elif isinstance(expr, W1.Not):
        e = flatten(expr.expr)
        return C0.Program(e.vars|{ans}, ans,
                          *(e.instrs + (C0.Assign(ans, C0.Not(C0.Var(e.retvar))),)))
    elif isinstance(expr, W1.And):
        l = flatten(expr.lhs)
        r = flatten(expr.rhs)
        return C0.Program({ans}|l.vars|r.vars, ans,
                          *(l.instrs + r.instrs +
                            (C0.Assign(ans, (C0.And(C0.Var(l.retvar),
                                                    C0.Var(r.retvar)))),)))
    elif isinstance(expr, W1.Or):
        l = flatten(expr.lhs)
        r = flatten(expr.rhs)
        return C0.Program({ans}|l.vars|r.vars, ans,
                          *(l.instrs + r.instrs +
                            (C0.Assign(ans, (C0.Or(C0.Var(l.retvar),
                                                   C0.Var(r.retvar)))),)))
    # Variables
    elif isinstance(expr, W1.Var):
        return C0.Program({expr.name}, expr.name)
    elif isinstance(expr, W1.Let):
        _vars = set()
        instrs = tuple()
        for binding in expr.bindings:
            _vars |= {binding}
            xe = flatten(expr.bindings[binding])
            _vars |= xe.vars
            instrs += xe.instrs
            instrs += (C0.Assign(binding, C0.Var(xe.retvar)),)
        be = flatten(expr.body)
        return C0.Program(_vars|be.vars, be.retvar, *(instrs + be.instrs))
    else:
        raise Exception('flatten: unrecognized expression type %s' % str(type(expr)))

def select_instr(prog):
    labels = {'read:':x86.Label('read:')}
    instrs = []
    for instr in prog.instrs:
        if isinstance(instr, C0.Assign):
            dest = instr.var
            subinstr = instr.expr
            if isinstance(subinstr, C0.Bool):
                instrs.append(x86.Movq(x86.Bool(-1 if subinstr.val else 0),
                                       x86.Var(dest)))
            elif isinstance(subinstr, C0.Read):
                instrs.append(x86.Callq(labels['read:']))
                instrs.append(x86.Movq(x86.Register('rax'),
                                       x86.Var(dest)))
            if isinstance(subinstr, C0.Var):
                instrs.append(x86.Movq(x86.Var(subinstr.name),
                                       x86.Var(dest)))
            # Operators
            elif isinstance(subinstr, C0.Not):
                instrs.append(x86.Movq(x86.Var(subinstr.expr.name),
                                       x86.Var(dest)))
                instrs.append(x86.Notq(x86.Var(dest)))
            # the following two assume lhs is not dest
            elif isinstance(subinstr, C0.And):
                instrs.append(x86.Movq(x86.Var(subinstr.rhs.name),
                                       x86.Var(dest)))
                instrs.append(x86.Andq(x86.Var(subinstr.lhs.name),
                                       x86.Var(dest)))
            elif isinstance(subinstr, C0.Or):
                instrs.append(x86.Movq(x86.Var(subinstr.rhs.name),
                                       x86.Var(dest)))
                instrs.append(x86.Orq(x86.Var(subinstr.lhs.name),
                                      x86.Var(dest)))
    instrs.append(x86.Movq(x86.Var(prog.retvar),
                           x86.Register('rax')))
    return x86.Program(*instrs, **labels)

def assign_homes(asm, variables):
    variables = list(variables)
    k = len(variables)
    k = k+1 if k%2 else k

    labels = asm.labels
    instrs = [x86.Pushq(x86.Register('rsi')),
              x86.Movq(x86.Register('rsi'), x86.Register('rbp')),
              x86.Subq(x86.Int(k), x86.Register('rsi'))
    ]

    def rename(arg):
        hat = variables.index(arg.name)
        return x86.Address(-hat*8, x86.Register('rbp'))
    for line in asm.body:
        args = tuple([rename(arg) if isinstance(arg, x86.Var) else arg
                      for arg in line])
        instrs.append(type(line)(*args))

    instrs += [x86.Addq(x86.Int(k), x86.Register('rsi')),
               x86.Popq(x86.Register('rbp'))]
    return x86.Program(*instrs, **labels)

def patch(asm):
    labels = asm.labels
    instrs = []

    def clashes(lhs, rhs):
        if (isinstance(lhs, x86.Address) and isinstance(rhs, x86.Address) and
            (lhs.register == rhs.register)):
            return True
        return False

    for instr in asm.body:
        if len(instr) == 2 and clashes(instr.lhs, instr.rhs):
            instrs += (x86.Movq(instr.lhs, x86.Register('rax')),
                       type(instr)(x86.Register('rax'), instr.rhs))
        else:
            instrs.append(instr)
    return x86.Program(*instrs, **labels)

def output(asm):
    return str(asm)

####

def pipeline(program):
    out = (program,)
    steps = (uniquify, flatten, select_instr, assign_homes, patch, output)

    variables = ()
    for step in steps:
        if step == assign_homes:
            program = step(program, variables)
        else:
            program = step(program)
        if step == flatten:
            variables = program.vars
        out += (program,)
    return out

if __name__ == '__main__':
    from W1 import *
    program = Program(Let({'a':Bool(True),'b':Bool(False)},
                          And(Or(Var('a'), Var('b')),
                              Or(Not(Var('a')), Not(Var('b'))))))
    results = pipeline(program)
    for result in results[:-1]:
        print(result)
        print(result.interpret())
    print(results[-1])
