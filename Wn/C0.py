
from collections import namedtuple
import Wsys

class Program:
    def __init__(self, vars, retvar, *instrs):
        self.vars = vars
        self.retvar = retvar
        self.instrs = instrs
    def __str__(self):
        return '(program (%s)\n\t%s\n\t(ret %s))' % (' '.join(self.vars),
                                              '\n\t'.join([str(instr)
                                                        for instr in self.instrs]),
                                              self.retvar)
    def interpret(self):
        env = dict.fromkeys(self.vars, False)
        for instr in self.instrs:
            instr.interpret(env)
        return env[self.retvar]

# Input
class Read(namedtuple('Read', '')):
    def __str__(self):
        return '(read)'
    def interpret(self, env):
        return Wsys.readTF()

# Type
class Bool(namedtuple('Bool', 'val')):
    def __str__(self):
        if self.val:
            return '-1'
        else:
            return '0'
    def interpret(self, env):
        return self.val

# Operations
class Not(namedtuple('Not', 'expr')):
    def __str__(self):
        return '(not %s)' % self
    def interpret(self, env):
        return not self.expr.interpret(env)

class And(namedtuple('And', 'lhs rhs')):
    def __str__(self):
        return '(and %s %s)' % self
    def interpret(self, env):
        lhs = self.lhs.interpret(env)
        rhs = self.rhs.interpret(env)
        return self.lhs.interpret(env) and self.rhs.interpret(env)

class Or(namedtuple('Or', 'lhs rhs')):
    def __str__(self):
        return '(or %s %s)' % self
    def interpret(self, env):
        return self.lhs.interpret(env) or self.rhs.interpret(env)

# Variables
class Var(namedtuple('Var', 'name')):
    def __str__(self):
        return self.name
    def interpret(self, env):
        return env[self.name]

class Assign(namedtuple('Assign', 'var expr')):
    def __str__(self):
        return '(:= %s %s)' % self
    def interpret(self, env):
        env[self.var] = self.expr.interpret(env)
        return env[self.var]
