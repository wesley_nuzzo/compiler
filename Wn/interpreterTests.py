
import unittest
from unittest import mock
import W1
import C0
import x86

class TestW1(unittest.TestCase):
    def setUp(self):
        def alternater():
            while True:
                yield True
                yield False
        alternater = mock.Mock(side_effect=alternater())
        alwaysTrue = mock.Mock(return_value = True)
        alwaysFalse = mock.Mock(return_value = False)

        self.genericExpr = mock.NonCallableMock()
        self.genericExpr.attach_mock(alternater, 'interpret')

        self.alwaysTrueExpr = mock.NonCallableMock()
        self.alwaysTrueExpr.attach_mock(alwaysTrue, 'interpret')
        self.alwaysFalseExpr = mock.NonCallableMock()
        self.alwaysFalseExpr.attach_mock(alwaysFalse, 'interpret')

        self.emptyEnv = {}
        self.xFalseEnv = {'x':False}
        self.xTrueEnv = {'x':True}

    def testRead(self):
        read = W1.Read()
        with mock.patch('Wsys.readTF', side_effect = [True, False]):
            self.assertTrue(read.interpret(self.emptyEnv))
            self.assertFalse(read.interpret(self.emptyEnv))

    def testBool(self):
        true = W1.Bool(True)
        false = W1.Bool(False)
        self.assertTrue(true.interpret(self.emptyEnv))
        self.assertFalse(false.interpret(self.emptyEnv))

    def testLet(self):
        let = W1.Let({'true':self.alwaysTrueExpr},
                     self.genericExpr)
        # Need to Add checks that subexprs are interpreted with correct envs
        self.assertEqual(let.interpret(self.emptyEnv), True)
        self.assertEqual(let.interpret(self.emptyEnv), False)

        self.assertEqual(let.interpret(self.xFalseEnv), True)
        self.assertEqual(let.interpret(self.xFalseEnv), False)

    def testVar(self):
        var = W1.Var('x')
        with self.assertRaises(W1.VarNotDefined):
            var.interpret(self.emptyEnv)
        self.assertEqual(var.interpret(self.xFalseEnv), False)

class TestC0(unittest.TestCase):
    pass

class Testx86(unittest.TestCase):
    pass

if __name__ == '__main__':
    unittest.main()
