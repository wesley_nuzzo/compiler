
def readTF():
    user = None
    while user not in ('t', 'T', 'f', 'F'):
        user = input('t/f: ')
    print()
    return True if user in ('t', 'T') else False


