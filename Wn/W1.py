
from collections import namedtuple
import Wsys

class VarNotDefined(Exception):
    pass

class Program(namedtuple('Program', 'body')):
    def __str__(self):
        return '(program %s)' % self
    def interpret(self):
        env = {}
        return self.body.interpret(env)

## Input
class Read(namedtuple('Read', '')):
    def __str__(self):
        return '(read)'
    def interpret(self, env):
        return Wsys.readTF()

## Data Type
class Bool(namedtuple('Bool', 'val')):
    def __str__(self):
        if self.val:
            return '(true)'
        return '(false)'
    def interpret(self, env):
        return self.val

## Operators
class Not(namedtuple('Not', 'expr')):
    def __str__(self):
        return '(not %s)' % self
    def interpret(self, env):
        return not self.expr.interpret(env)

class And(namedtuple('And', 'lhs rhs')):
    def __str__(self):
        return '(and %s %s)' % self
    def interpret(self, env):
        lhs = self.lhs.interpret(env)
        rhs = self.rhs.interpret(env)
        return self.lhs.interpret(env) and self.rhs.interpret(env)

class Or(namedtuple('Or', 'lhs rhs')):
    def __str__(self):
        return '(or %s %s)' % self
    def interpret(self, env):
        return self.lhs.interpret(env) or self.rhs.interpret(env)

## Frames
class Var(namedtuple('Var', 'name')):
    def __str__(self):
        return self.name
    def interpret(self, env):
        try:
            return env[self.name]
        except KeyError:
            raise VarNotDefined

class Let(namedtuple('Let', 'bindings body')):
    def __str__(self):
        bindingStr = ' '.join('[%s %s]' % (binding, self.bindings[binding])
                              for binding in self.bindings)
        return '(let (%s) %s)' % (bindingStr, self.body)
    def interpret(self, env):
        newEnv = env.copy()
        for binding in self.bindings:
            newEnv[binding] = self.bindings[binding].interpret(env)
        out = self.body.interpret(newEnv)
        return out

#### Main ####
if __name__ == '__main__':
    program = Program(Let({'a':Bool(True),'b':Bool(False)},
                          And(Or(Var('a'), Var('b')),
                              Or(Not(Var('a')), Not(Var('b'))))))
    print(program)
    print(program.interpret())
